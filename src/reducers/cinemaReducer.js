import actionTypes from '../actions/actionTypes';
import actions from '../actions/index';
import stateMachine from '../helpers/cinemaStateMachine';
import theMovieDbHelper from '../helpers/theMovieDbHelper';

const REDUX_INIT_STATE = "@@INIT";

const createInitState = function() {
    return {
        appState: stateMachine.INIT,
    }
};

function queryNowPlayingAPI (asyncDispatcher, state) {
        theMovieDbHelper.setup();
        theMovieDbHelper.getNowPlaying(null, function successCB(data) {
            asyncDispatcher(actions.addQueryMovieDbGetNowPlayingReceive(JSON.parse(data)));
        }, function errorCB(data) {
            asyncDispatcher(actions.addQueryMovieDbGetNowPlayingError(JSON.parse(data)));
        });
    return {
        ...state,
        appState: stateMachine.MOVIES_LOADING
    };
}

const queryNowPlayingError = function (state, data) {
    alert("Error loading movies \n " + data);
};

const queryNowPlayingArrived = function (state, data) {
    let newState = stateMachine.MOVIES_LOADING === state.appState ? stateMachine.MOVIES_DISPLAYING : state;
    return {
        ...state,
        appState: newState,
        movies: data
    }
};

const selectSceneTicketsSelection = function (state, data) {
    return {
        ...state,
        appState: stateMachine.TICKETS_SELECTION,
        currentMovie: data
    }
};

const selectedTickets = function (state, data) {
    return {
        ...state,
        appState: stateMachine.CONFIRM_CHECKOUT,
        currentTickets: data
    }
};

const confirmTickets = function (state, data) {
    return {
        ...state,
        appState: stateMachine.COMPLETE
    }
};

const selectSceneMovieDisplay = function (state) {
    return {
        ...state,
        appState: stateMachine.MOVIES_DISPLAYING
    }
};

const theMovieDbReducer = (state = {}, action) => {
    switch (action.type) {
        case REDUX_INIT_STATE:
            return createInitState();
        case actionTypes.QUERY_API_GET_NOW_PLAYING:
            return queryNowPlayingAPI(action.asyncDispatcher);
        case actionTypes.QUERY_API_GET_NOW_PLAYING_RECEIVE:
            return queryNowPlayingArrived(state, action.data);
        case actionTypes.QUERY_API_GET_NOW_PLAYING_ERROR:
            return queryNowPlayingError(state, action.data);
        case actionTypes.SELECTED_MOVIE:
            return selectSceneTicketsSelection(state, action.data);
        case actionTypes.SELECTED_TICKETS:
            return selectedTickets(state, action.data);
        case actionTypes.CONFIRMED_TICKETS:
            return confirmTickets(state);
        case actionTypes.COMPLETE_DISMISSED:
            return selectSceneMovieDisplay(state);
        case actionTypes.BACK_FROM_CONFIRM:
            return selectSceneTicketsSelection(state, state.currentMovie);
        case actionTypes.BACK_FROM_TICKET_SELECT:
            return selectSceneMovieDisplay(state);
        default:
            return state
    }
};

export default theMovieDbReducer;
