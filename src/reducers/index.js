import { combineReducers } from 'redux'
import cinemaReducer from './cinemaReducer';

export default combineReducers({
    cinemaReducer
})
