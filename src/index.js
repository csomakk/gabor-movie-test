import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { createStore, compose } from 'redux'
import App from './components/App'
import rootReducer from './reducers'
import * as serviceWorker from './serviceWorker'

const reduxDevToolsEnhancer = function () {
    return window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
};

const store = createStore(
    rootReducer,
    compose(
        reduxDevToolsEnhancer()
    )
);

ReactDOM.render(
    <App store={store}/>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
