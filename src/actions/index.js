import actionTypes from './actionTypes';

const actions = {
    addQueryMovieDbGetNowPlaying : asyncDispatch => ({
        type: actionTypes.QUERY_API_GET_NOW_PLAYING,
        asyncDispatcher: asyncDispatch
    }),
    
    addQueryMovieDbGetNowPlayingReceive : data => ({
        type: actionTypes.QUERY_API_GET_NOW_PLAYING_RECEIVE,
        data: data
    }),    
    addQueryMovieDbGetNowPlayingError : error => ({
        type: actionTypes.QUERY_API_GET_NOW_PLAYING_ERROR,
        data: error
    }),
};

export default actions;