import React, { Component } from 'react';
import './App.css';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import TicketSummariser from '../helpers/TicketSummariser';
import ActionTypes from '../actions/actionTypes'


class ConfirmationScreeen extends Component {
    onConfirmClick() {
        this.props.dispatcher({
            type: ActionTypes.CONFIRMED_TICKETS
        });
    }   
    
    onBackClick() {
        this.props.dispatcher({
            type: ActionTypes.BACK_FROM_CONFIRM
        });
    }
    
    render() {
        let filteredTickets = [];
        for (let key in this.props.selectedTickets) {
            const value = this.props.selectedTickets[key];
            if (value > 0) {
                filteredTickets.push(
                    "" + value + " " + key + " ticket" + (value > 1 ? "s" : "") 
                )
            }
        }
        
        const formattedTickets = filteredTickets.join(` and `);
        
        return (
          <div className="App-content ticketSelect">
              <h2>CONFIRM</h2>
              {formattedTickets} to
              <h3>{this.props.movie.title}</h3>
              <h4>
              for: {TicketSummariser.totalFromSelectedTickets(this.props.selectedTickets)}
              </h4>
              <div>
                  <button className="backButton btn btn-light"  onClick={this.onBackClick.bind(this)}>
                      ‹ Back
                  </button>
                  <button className="proceedButton btn btn-primary" onClick={this.onConfirmClick.bind(this)}> Confirm › </button>
              </div>
          </div>
        );
  }
}

export default ConfirmationScreeen;
