import React, { Component } from 'react';
import './App.css';
import actions from '../actions/index';
import SelectMovieScreen from './SelectMovieScreen';
import ConfirmationScreen from './ConfirmationScreen';
import TicketSelectScreen from './TicketSelectScreen';
import CompleteScreen from './CompleteScreen';
import stateMachine from '../helpers/cinemaStateMachine';
import TicketOptions from '../helpers/TicketOptions';
import LoadingScreen from "./LoadingScreen";

class App extends Component {

  componentWillMount() {
      this.setState({appState: null});
      this.props.store.dispatch(actions.addQueryMovieDbGetNowPlaying(this.props.store.dispatch));
      let thisRef = this;
      this.props.store.subscribe(function() {
          thisRef.setState(thisRef.props.store.getState().cinemaReducer);
      });
      
      this.ticketOptions = TicketOptions;
  }
  
  static renderLoading() {
      return (<LoadingScreen />);
  }
  
  renderMoviesScreen() {
      console.log("renderMoviesScreen");
      return (
          <SelectMovieScreen
              dispatcher={this.props.store.dispatch}
              movies={this.state.movies}>
          </SelectMovieScreen>
      );
  }
  
  renderTicketsSelection() {
      return (
          <TicketSelectScreen
              dispatcher={this.props.store.dispatch}
              movie={this.state.currentMovie}
              ticketOptions={this.ticketOptions}
              selectedTickets={this.state.currentTickets}
              >
          </TicketSelectScreen>
      );
  }

  renderConfirmationScreen() {
      return (
          <ConfirmationScreen
              dispatcher={this.props.store.dispatch}
              movie={this.state.currentMovie}
              ticketOptions={this.ticketOptions}
              selectedTickets={this.state.currentTickets}
          >
          </ConfirmationScreen>
      );
  }
    
  renderCompleteScreen() {
      return (
          <CompleteScreen
              dispatcher={this.props.store.dispatch}
              movie={this.state.currentMovie}
              ticketOptions={this.ticketOptions}
              selectedTickets={this.state.currentTickets}
          >
          </CompleteScreen>
      );
  }
  
  renderContent() {
      switch (this.state.appState) {
          case stateMachine.MOVIES_DISPLAYING: return this.renderMoviesScreen();
          case stateMachine.TICKETS_SELECTION: return this.renderTicketsSelection();
          case stateMachine.CONFIRM_CHECKOUT: return this.renderConfirmationScreen();
          case stateMachine.COMPLETE: return this.renderCompleteScreen();
          default: return App.renderLoading();
      }
  }
  
  render() {
      return <div className="App-main">
          <h1 className="header">Fun Cinema Booking Portal</h1>
          {this.renderContent()}
      </div>
  }
}

export default App;
