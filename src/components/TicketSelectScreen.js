import React, { Component } from 'react';
import './App.css';
import TicketSummariser from '../helpers/TicketSummariser';
import ActionTypes from '../actions/actionTypes'
import NumericInput from 'react-numeric-input';

class TicketSelectScreen extends Component {
  componentWillMount() {
      this.setState({
          total: ""
      });
  }  
  componentDidMount() {
     this.updateTotal();
  }

  onBackClick() {
      this.props.dispatcher({
          type: ActionTypes.BACK_FROM_TICKET_SELECT
      });
  }

  updateTotal() {
      let selectedPerKey = {};
      for (let key in this.props.ticketOptions) {
          const inputForKey = this.refs["ticketOption" + key];
          console.log("inputForKey", key, inputForKey.state.value)
          selectedPerKey[key] = parseInt(inputForKey.state.value) || 0;
      }
      this.setState({
          total: TicketSummariser.totalFromSelectedTickets(selectedPerKey), 
          totalForKeys: selectedPerKey
      });
  }
  
  renderTicketOptions() {
      let rows = [];

      for (let key in this.props.ticketOptions) {
          let defaultValue = 0;
          if (!!this.props.selectedTickets) {
              defaultValue = this.props.selectedTickets[key] || 0;
          }
          rows.push(<div className="ticketOption" key={"ticketOptionDiv" + key}>
              <label className="ticketOptionLabel">{key}</label>
              <NumericInput 
                  mobile 
                  ref={"ticketOption" + key} 
                  id={"ticketOption" + key} 
                  key={"ticketOption" + key} 
                  onChange={this.updateTotal.bind(this)} 
                  defaultValue={defaultValue} 
                  type="number" 
                  min={0} 
              />
          </div>)
      }
      
      return (
          <div>
              {rows}
          </div>
      )
  }
  
  onProceedClick() {
      if (this.state.total === TicketSummariser.totalFromSelectedTickets({})) {
          alert("0 tickets selected");
          return;
      }
      this.props.dispatcher({
          type: ActionTypes.SELECTED_TICKETS, 
          data: this.state.totalForKeys
      });
  }
  
  render() {
      return (
          <div className="App-content ticketSelect">
              <h2>
              Select tickets for {this.props.movie.title}
              </h2>
              
              {this.renderTicketOptions()}
              
              Total: <span id="total">{this.state.total}</span>
              
              <div>
                  <button className="backButton btn btn-light"  onClick={this.onBackClick.bind(this)}>
                      ‹ Back
                  </button>
                <button className="proceedButton btn btn-primary" onClick={this.onProceedClick.bind(this)}> Proceed › </button>
              </div>
          </div>
      );
  }
}

export default TicketSelectScreen;
