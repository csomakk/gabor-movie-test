import React from 'react';
import ReactDOM from 'react-dom';
import TicketSelectScreen from './TicketSelectScreen';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TicketOptions from '../helpers/TicketOptions';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TicketSelectScreen movie={{title: "SampleTitle"}} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('with 0 tickets selected it wont proceed ', () => {
  Enzyme.configure({ adapter: new Adapter() });
  let dispatcherCalls = [];
  let ticketOptions = TicketOptions;
  let alertLogs = [];
  window.alert = (msg) => { alertLogs.push(msg); };
  const component = mount(
      <TicketSelectScreen 
        movie={{title: "SampleTitle2"}}
        dispatcher={function (args) {
            dispatcherCalls.push(args)
        }}
        ticketOptions={ticketOptions}
        selectedTickets={{}}
      />);
  
  
  expect(component.state().total).toEqual("£0.00");

  expect(dispatcherCalls.length).toEqual(0);
  component.find('.backButton').simulate('click');
  expect(dispatcherCalls.pop()).toEqual( { type: 'BACK_FROM_TICKET_SELECT' });
  
  component.find('.proceedButton').simulate('click');
  expect(dispatcherCalls.length).toEqual(0);
  expect(alertLogs.pop()).toEqual( "0 tickets selected" );
  component.unmount();

});

it('default tickets selelcted fine ', () => {
    Enzyme.configure({ adapter: new Adapter() });
    let ticketOptions = TicketOptions;
    const component = mount(
        <TicketSelectScreen
            movie={{title: "SampleTitle2"}}
            ticketOptions={ticketOptions}
            selectedTickets={{"Adult" : 3 }}
        />);

    expect(component.state().total).toEqual("£30.00");
});



it('selecting tickets and proceeding works ', () => {
    Enzyme.configure({ adapter: new Adapter() });
    let dispatcherCalls = [];
    let ticketOptions = TicketOptions;
    const component = mount(
        <TicketSelectScreen
            movie={{title: "SampleTitle2"}}
            dispatcher={function (args) {
                dispatcherCalls.push(args)
            }}
            ticketOptions={ticketOptions}
        />);


    component.ref("ticketOptionAdult").state.value = 2;
    component.instance().updateTotal();

    expect(component.state().total).toEqual("£20.00");

    component.find('.proceedButton').simulate('click');
    expect(dispatcherCalls.pop()).toEqual( {"data": {"Adult": 2, "Child": 0, "Concession": 0}, "type": "SELECTED_TICKETS"});

});

