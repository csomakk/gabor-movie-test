import React, { Component } from 'react';
import './App.css';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import theMovieDbHelper from '../helpers/theMovieDbHelper';
import ActionTypes from '../actions/actionTypes'

class SelectMovieScreen extends Component {
  componentDidMount() {
      this.setupGrid();
  }
  
  setupGrid() {
      this.refs.grid.items = this.props.movies.results;
      
      const posterColumn = document.querySelector('#posterColumn');
      posterColumn.renderer = this.renderPosterColumn;
      
      const proceedColumn = document.querySelector('#proceedColumn');
      proceedColumn.renderer = this.renderProceedColumn;
  }
  
  renderPosterColumn = (root, column, rowData) => {
      const path = theMovieDbHelper.getFullImagePath(rowData.item.poster_path, 185);
      const img = document.createElement("IMG");
      img.src = path;
      img.alt = rowData.item.title;
      img.className = "posterImage";
      root.innerHTML = img.outerHTML;
  };
  
  renderProceedColumn = (root, column, rowData) => {
      const button = document.createElement("button");
      button.innerText = "Book Tickets ›";
      button.className = "bookTicketsButton btn-primary";
      root.innerHTML = button.outerHTML;
      let thisRef = this;
      root.onclick = function() {
          thisRef.props.dispatcher({type: ActionTypes.SELECTED_MOVIE, data: rowData.item});
      };
  };
  
  render() {
      return (
          <div className="App-content movies">
              <vaadin-grid ref="grid" >
                  <vaadin-grid-column id="proceedColumn" width="30%"  />
                  <vaadin-grid-column path="title" header="Title" />
                  <vaadin-grid-column id="posterColumn" header="Poster" />
                  <vaadin-grid-column path="popularity" header="Popularity" />
              </vaadin-grid>
          </div>
      );
  }
}

export default SelectMovieScreen;
