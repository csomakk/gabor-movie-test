import React, { Component } from 'react';
class LoadingScreen extends Component {
    render() {
      return (
          <div className="App-content loadingScreen">
              <div class="spinner-border text-primary" role="status">
                  <span class="sr-only">Loading...</span>
              </div>
          </div>
      );
  }
}

export default LoadingScreen;
