import React, { Component } from 'react';
import './App.css';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import ActionTypes from '../actions/actionTypes'

class CompleteScreen extends Component {
    onConfirmClick() {
        this.props.dispatcher({
            type: ActionTypes.COMPLETE_DISMISSED
        });
    }
    
    render() {
      return (
          <div className="App-content completeScreen" >
              <h2>
                Order successfully completed.
              </h2>
              <button className="btn btn-primary completeDismissButton" onClick={this.onConfirmClick.bind(this)}> Dismiss › </button>
          </div>
      );
  }
}

export default CompleteScreen;
