const TicketOptions = {
    "Adult": 10,
    "Child": 7.5,
    "Concession": 5
};

export default TicketOptions;