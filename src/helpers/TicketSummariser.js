import TicketOptions from './TicketOptions';

const TicketSummariser = {
    totalFromSelectedTickets(selected) {
        let total = 0;
        for (let key in selected) {
            let priceForKey = TicketOptions[key];
            total += selected[key] * priceForKey;
        }
    
        return this.formatToCurrency(total);
    },
    
    formatToCurrency(value) {
        return new Intl.NumberFormat('en-GB', {style: 'currency', currency: 'GBP'}).format(value);
    }
};

export default TicketSummariser;