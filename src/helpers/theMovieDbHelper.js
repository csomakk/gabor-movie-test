import theMovieDbJS from 'themoviedb-javascript-library'

const theMovieDbHelper = {
    setup() {
        theMovieDbJS.common.api_key = '51946da79c2bbf26bca17081a69c8ca7';
    },

    getNowPlaying(options, success, error) {
        theMovieDbJS.movies.getNowPlaying(options, success, error);
    },
    
    getFullImagePath(relativePath, width) {
        return theMovieDbJS.common.images_uri + "w" + width +  relativePath;
    }
};

export default theMovieDbHelper;
