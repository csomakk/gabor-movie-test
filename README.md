JS + React + Redux app for movie theather booking system.

## Available Scripts
### `npm install` 
-- in case this fails, try node version < 10. ex. $ nvm use 6
### `npm start`
### `npm test`
### `npm run build`

## explanation of choices 
### create-react-app:
- it was convenient to start with. For the long run i'd probably eject.
 
### Redux: 
- I didn't want to go with bare, because API calls with bare react style is not my favourite.
- Also I really like the debug tool and the start state-s were useful when designing each page.

### themoviedb js lib: 
- it made sense to use existing 3rd party library to get code completion and faster extension possibilities.
- also I only query it once, assuming clients are turned off for the night. 
- the response is in popularity order, what displayed works fine for a movie counter
- from https://github.com/cavestri/themoviedb-javascript-library

### api key in code
- since request is made with that in url, we can't really keep it safe. we should move to user based auth. 

### vaadin grid:
- On the interview I mentioned that Vaadin and React could live together side by side, providing a 
better transitional period for the migration. So I used it to prove the point of vaadin and react living together.
- Also it uses webcomponents / polymer, so this would make babel and polyfills harder if we'd need to support older devices.
- from https://vaadin.com/components/vaadin-grid/html-examples/grid-basic-demos

### unit tests
- just small example of Enzime, for Ticket Select Screen

### CSS and bootstrap
- This app is really simple, so a CSS preprocessor didn't make sense to me. Not even an organised css file.
- I used some bootstrap to make it look nice quickly.
- Themeing probably would be a good idea here, that would allow B2B rebranding possibilities.
 
### Mobile
- I made sure app is usable on mobile, for opening up the site to external custumers in the future.

### no navigator
- Hosting is harder with a navigator and it will limit B2B options or if we're served in an iframe for any reason

### alerts
- I could've used bootstrap to display error, but provided users will be educated I'm happy with a simple alert.

### cinemaStateMachine
- quite easy to identify states, possibly wire in url navigator for it, and with the reducer its easy to follow the possible flows.

### provider/consumer
I prefer directly providing needed props for few reasons: 
-  more readable, code navigateable
- unit testable
- closer to vanilla react

While only downside is a bit of boilerplate, and a longer App class.

### react-numeric-input
quick way to provide much better ux for ticket select screen

## todo list:
- Adding Analytics / Tracking / Logging
- cachebust : will need to add in case we'd have to rerelease.
- build server + continous delivery